package be.c4j.demo;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

public final class PMF {
    private static final PersistenceManagerFactory pmfInstance =
            JDOHelper.getPersistenceManagerFactory("todo");

    private PMF() {}

    public static PersistenceManagerFactory get() {
        return pmfInstance;
    }
}
