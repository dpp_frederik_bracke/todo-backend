package be.c4j.demo.service;

import be.c4j.demo.Constants;
import be.c4j.demo.PMF;
import be.c4j.demo.model.Todo;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;

import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.List;

@Api(
        name="todo",
        version="v1",
        clientIds = { Constants.WEB_CLIENT_ID }
)
public class TodoService {

    @ApiMethod(name = "todo.save", httpMethod = HttpMethod.POST)
    public Todo saveTodo(Todo todo) {
        PersistenceManager mgr = getPersistenceManager();
        try {
            mgr.makePersistent(todo);
        } finally {
            mgr.close();
        }
        return todo;
    }

    @ApiMethod(name = "todo.list", httpMethod = HttpMethod.GET)
    public List<Todo> getTodos() {
        PersistenceManager mgr = getPersistenceManager();
        List<Todo> todoList;

        try {
            Query todos = mgr.newQuery(Todo.class);
            todoList = (List<Todo>)todos.execute();
        } finally {
            mgr.close();
        }
        return todoList;
    }

    @ApiMethod(name = "todo.get", httpMethod = HttpMethod.GET)
    public Todo getTodo(@Named("id") Long id){
        PersistenceManager mgr = getPersistenceManager();
        Todo todo;
        try {
            todo = mgr.getObjectById(Todo.class, id);
        } finally {
            mgr.close();
        }
        return todo;
    }


    private static PersistenceManager getPersistenceManager() {
        return PMF.get().getPersistenceManager();
    }
}
